package com.ada.artikel.model.response.home


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Source(
        @Expose
        @SerializedName("id")
        val id: Any,
        @Expose
        @SerializedName("name")
        val name: String
)