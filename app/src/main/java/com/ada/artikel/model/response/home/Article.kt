package com.ada.artikel.model.response.home


import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Article(
        @Expose
        @SerializedName("author")
        val author: String,
        @Expose
        @SerializedName("content")
        val content: String,
        @Expose
        @SerializedName("description")
        val description: String,
        @Expose
        @SerializedName("publishedAt")
        val publishedAt: String,
        @Expose
        @SerializedName("source")
        val source: String,
        @Expose
        @SerializedName("title")
        val title: String,
        @Expose
        @SerializedName("url")
        val url: String,
        @Expose
        @SerializedName("urlToImage")
        val urlToImage: String
) : Parcelable