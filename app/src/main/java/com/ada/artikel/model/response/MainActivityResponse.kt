package com.ada.artikel.model.response


import com.ada.artikel.model.response.home.Article
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class MainActivityResponse(
        @Expose
        @SerializedName("articles")
        val articles: List<Article>,
        @Expose
        @SerializedName("status")
        val status: String,
        @Expose
        @SerializedName("totalResults")
        val totalResults: Int
)