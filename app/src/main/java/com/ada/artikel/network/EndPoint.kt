package com.ada.artikel.network

import com.ada.artikel.model.response.MainActivityResponse
import io.reactivex.Observable
import retrofit2.http.*

interface EndPoint {
    @GET("everything")
    fun article(
            @Query("q") q: String,
            @Query("sortBy") sortBy: String,
            @Query("apiKey") apiKey: String
    ): Observable<MainActivityResponse>
}