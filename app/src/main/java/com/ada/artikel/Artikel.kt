package com.ada.artikel

import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import androidx.multidex.MultiDexApplication
import com.ada.artikel.network.HttpClient

class Artikel : MultiDexApplication() {
    companion object{
        lateinit var instance : Artikel

        fun getApp() : Artikel{
            return instance
        }
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    fun getPreferences() : SharedPreferences{
        return PreferenceManager.getDefaultSharedPreferences(this)
    }
}