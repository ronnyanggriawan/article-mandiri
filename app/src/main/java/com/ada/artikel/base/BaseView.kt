package com.ada.artikel.base

interface BaseView {
    fun showLoading()
    fun dismissLoading()
}