package com.ada.artikel.base

interface BasePresenter {
    fun subscribe()

    fun unSubscribe()
}