package com.ada.artikel.ui.home

import com.ada.artikel.base.BasePresenter
import com.ada.artikel.base.BaseView
import com.ada.artikel.model.response.MainActivityResponse
import com.ada.artikel.model.response.home.Article

interface HomeContract {

    interface View : BaseView {
        fun onCustomerSucces(mainActivityResponse: List<Article>, message: String)
        fun onHomeFailed(message: String)
    }

    interface Presenter : HomeContract, BasePresenter {
        fun getCustomer(q: String, sortBy: String, apiKey: String)
    }
}