package com.ada.artikel.ui.home

import android.util.Log
import com.ada.artikel.network.HttpClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class HomePresenter(private val view: HomeContract.View) : HomeContract.Presenter {
    private val mCompositeDisposable: CompositeDisposable?

    init {
        this.mCompositeDisposable = CompositeDisposable()
    }

    override fun getCustomer(q: String, sortBy: String,apiKey:String) {
        view.showLoading()
        mCompositeDisposable!!.add(HttpClient.getInstance().getApi()!!.article(q,sortBy,apiKey)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    view.dismissLoading()

                    if (it.status == "ok") {

                        it.articles?.let { it1 -> view.onCustomerSucces(it1,it.status) }
                    } else {
                        it.status?.let { it1 -> view.onHomeFailed(it1) }
                    }
                },
                {
                    view.dismissLoading()
                    view.onHomeFailed(it.message.toString())
                }
            ))
    }

    override fun subscribe() {
    }

    override fun unSubscribe() {
        mCompositeDisposable!!.clear()
    }

}