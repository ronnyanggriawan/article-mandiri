package com.ada.artikel.ui.home

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ada.artikel.R
import com.ada.artikel.model.response.home.Article
import com.google.android.material.snackbar.Snackbar

class HomeActivity : AppCompatActivity(), HomeContract.View,HomeAdapter.ItemAdapterCallback {
    var progressDialog: Dialog? = null
    private lateinit var rvHome : RecyclerView
    private lateinit var lLHome : LinearLayout
    private lateinit var presenter: HomePresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
    }

    private fun initView() {
        presenter = HomePresenter(this)
        presenter.getCustomer("Apple","popularity","bdc37e4611bc46978ddeceb6b047b219")
        progressDialog = Dialog(this)
        val dialogLayout = layoutInflater.inflate(R.layout.dialog_laoder, null)
        progressDialog?.let {
            it.setContentView(dialogLayout)
            it.setCancelable(false)
            it.window?.setBackgroundDrawableResource(android.R.color.transparent)
        }
        lLHome = findViewById(R.id.ll_home)
        rvHome = findViewById(R.id.rc_home)
    }

    override fun onCustomerSucces(mainActivityResponse: List<Article>, message: String) {
        var adapter = HomeAdapter(mainActivityResponse, this)
        var layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        rvHome.layoutManager = layoutManager
        rvHome.adapter = adapter
    }

    override fun onHomeFailed(message: String) {
        Snackbar.make(lLHome, message, Snackbar.LENGTH_LONG).show()
    }

    override fun showLoading() {
        progressDialog?.show()
    }

    override fun dismissLoading() {
        progressDialog?.dismiss()
    }

    override fun onCllick(v: View, data: Article) {

    }
}