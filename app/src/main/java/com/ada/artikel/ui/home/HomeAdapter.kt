package com.ada.artikel.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ada.artikel.databinding.ListItemArticleBinding

import com.ada.artikel.model.response.home.Article
import com.bumptech.glide.Glide

class HomeAdapter(private var items: List<Article>,
                  private val itemAdapterCallback: ItemAdapterCallback) :RecyclerView.Adapter<HomeAdapter.Holder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        context = parent.context;
        val navItem = ListItemArticleBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return Holder(navItem)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val item: Article = items[position]
        holder.bind(item,itemAdapterCallback)
    }

    class Holder(private val binding: ListItemArticleBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(data: Article, itemAdapterCallback: ItemAdapterCallback) {
            itemView.apply {
                binding.tvAuthor.text = data?.author
                binding.tvTitle.text = data?.title
                binding.tvDesc.text = data?.description
                Glide.with(context)
                    .load(data.urlToImage)
                    .into(binding.imgArticle)

                itemView.setOnClickListener {
                    itemAdapterCallback.onCllick(it,data)
                }
            }
        }
    }
    interface ItemAdapterCallback{
        fun onCllick(v: View, data:Article)
    }
}